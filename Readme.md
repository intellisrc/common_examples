# About this repository

These are some example usages of the [common library](https://gitlab.com/intellisrc/common/).

# How to run the examples

1. Clone this repository and open the examples in your favorite IDE (we recommend IntelliJ). 
2. Synchronize Gradle
3. Then, go to the example and run it.

Be aware that because all modules are being used, it will download all dependencies, which may take time.

# Contribuiting

Please feel free to contribute with examples in groovy. 
Using `@CompileStatic` in the examples is not a requirement but helps to make it more strict.
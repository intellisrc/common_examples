package com.intellisrc.common_examples.core

import com.intellisrc.core.Log
import groovy.transform.CompileStatic

/**
 * @since 5/11/20.
 */
@CompileStatic
class LogSimple {
    static void main(String[] args) {
        if(args.size() > 0) {
            Log.i("This is information: %s", args.first())
        } else {
            Log.w("No arguments were passed")
        }
    }
}
